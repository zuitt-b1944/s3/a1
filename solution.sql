-- S3 ACTIVITY
-- STEP 1: Add following records to the USERS table.
	INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");
	SELECT * FROM users;


-- STEP 2: Add following records to the POSTS table.
	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("First Code", "Hello World!", "2021-01-02 01:00:00", 1);	
	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Second Code", "Hello Earth!", "2021-01-02 02:00:00", 1);
	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Third Code", "Welcome to Mars!", "2021-01-02 03:00:00", 2);
	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Fourth Code", "Bye Bye Solar System!", "2021-01-02 04:00:00", 4);
	SELECT * FROM posts;


-- STEP 3: Get all posts with an Author ID of 1.
	SELECT content FROM posts WHERE author_id = 1;


-- STEP 4: Get all user's email and datetime of creation
	SELECT email, datetime_created FROM users;


-- STEP 5: Update a post's content to "Hello to the people of the Earth!" by using the record's ID.
	SET FOREIGN_KEY_CHECKS=0;
	UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 7;
	SELECT * FROM posts;


-- STEP 6: Delete the user with an email of "johndoe@gmail.com"
	DELETE FROM users WHERE email = "johndoe@gmail.com";
	SELECT * FROM users;